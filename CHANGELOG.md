###Cambios

- Añadido soporte para registro y login (16/05/2017)
- Actualizado soporte para paginas
- Añadido soporte para siguiente post y anterior
- Actualización a versión 3.0.0 (Compatibilidad con Plugin Oficial)
- Añadido soporte para imágenes destacadas 27/06/2016
- Añadido soporte para custom field 30/06/2016
- Añadido opción de paginación en HOME y CATEGORIAS 03/07/2016
